class AddLoan extends React.Component {
    constructor(props) {
		super(props);
		this.state = {
		    lender: "",
		    borrower: "",
		    amount: ""
		};
		this.handleChangeLender = this.handleChangeLender.bind(this)
		this.handleChangeBorrower = this.handleChangeBorrower.bind(this)
		this.handleChangeAmount = this.handleChangeAmount.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}

    getInitialState() {
        return {
            name: ''
        };
    }

    handleChangeLender(val) {
        this.setState({lender: val.target.value});
    }

    handleChangeBorrower(val) {
        this.setState({borrower: val.target.value});
    }

    handleChangeAmount(val) {
        this.setState({amount: val.target.value});
    }

    handleSubmit(val) {
        var self = this;
    
        fetch('/addloan/' + this.state.lender + "/" + this.state.borrower + "/" + this.state.amount) 
        .then(function(response) {
            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +  response.status);
                return;
            }
        })
        .catch(function(error) {
            console.log('Fetch Error: ', error);
        });
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                <label>Lender ID</label>
                <input className="form-control" type="text" value={this.state.lender} onChange={this.handleChangeLender} />
                </div>
                <div className="form-group">
                <label>Borrower ID</label>
                <input className="form-control" type="text" value={this.state.name} onChange={this.handleChangeBorrower} />
                </div>
                <div className="form-group">
                <label>Amount</label>
                <input className="form-control" type="text" value={this.state.amount} onChange={this.handleChangeAmount} />
                </div>
                <button type="button" onClick={this.handleSubmit} >Submit</button>
            </form>
        );
    }
}

class AddPerson extends React.Component {
    constructor(props) {
		super(props);
		this.state = {name: []};
		this.handleChangeName = this.handleChangeName.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}

    getInitialState() {
        return {
            name: ''
        };
    }

    handleChangeName(val) {
        this.setState({name: val.target.value});
    }

    handleSubmit(val) {
        var self = this;
    
        fetch('/addperson/' + this.state.name) 
        .then(function(response) {
            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +  response.status);
                return;
            }
        })
        .catch(function(error) {
            console.log('Fetch Error: ', error);
        });
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                Name:
                <input type="text" value={this.state.name} onChange={this.handleChangeName} />
                </label>
                <button type="button" onClick={this.handleSubmit} >Submit</button>
            </form>
        );
    }
}

class NavigationBar extends React.Component {
    /* Weird - if I put it into react's component, then those two buttons are very close ... */
    render() {
        return (
            <nav className="navbar navbar-inverse navbar-static-top">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">Loans Collector</a>
                    </div>
                    <ul className="nav navbar-nav">
                        <li>
                            <a href={"?listloans"}>
                            <span className="glyphicon glyphicon-euro"></span> List of loans
                            </a>
                        </li>
                        <li>
                            <a href={"?listpersons"}>
                            <span className="glyphicon glyphicon-user"></span> List of people
                            </a>
                        </li>
                    </ul>
                    <a href="?addloan" className="btn btn-default navbar-btn">
                    <span className="glyphicon glyphicon-pencil"></span> Add loan
                    </a>
                    <a href="?addperson" className="btn btn-default navbar-btn">
                    <span className="glyphicon glyphicon-plus"></span> Add person
                    </a>
                </div>
            </nav>
        );
    }
}

class ListPersons extends React.Component {
    constructor(props) {
		super(props);
		this.state = {serverRequest: []};
	}

    componentDidMount() {
        var self = this;
    
        fetch('/getpersons/') 
        .then(function(response) {
            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +  response.status);
                return;
            }

            response.json().then(function(jsonResponse) {
                console.log("jsonResponse");
                console.log(jsonResponse)
                self.setState({serverRequest: jsonResponse});
            });
        })
        .catch(function(error) {
            console.log('Fetch Error: ', error);
        });
	}

    render() {
        var self = this;
        console.log("displaying");
        console.log(self.state.serverRequest);
        var table = <h5>Loading</h5>;
        if (self.state.serverRequest instanceof Array) {
            table = self.state.serverRequest.map((row) =>
                <tr>
                    <td>{row.id}</td>
                    <td>{row.name}</td>
                </tr>
            );
        }

        console.log("table");
        console.log(table);
        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {table}
                </tbody>
            </table>
        );
    }
}

class ListLoans extends React.Component {
    constructor(props) {
		super(props);
		this.state = {serverRequest: []};
	}

    componentDidMount() {
        var self = this;
    
        fetch('/getloans/') 
        .then(function(response) {
            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +  response.status);
                return;
            }

            response.json().then(function(jsonResponse) {
                console.log("jsonResponse");
                console.log(jsonResponse)
                self.setState({serverRequest: jsonResponse});
            });
        })
        .catch(function(error) {
            console.log('Fetch Error: ', error);
        });
	}

    render() {
        var self = this;
        console.log("displaying");
        console.log(self.state.serverRequest);
        var table = <h5>Loading</h5>;
        if (self.state.serverRequest instanceof Array) {
            table = self.state.serverRequest.map((row) =>
                <tr>
                    <td>{row.id}</td>
                    <td>{row.borrower}</td>
                    <td>{row.lender}</td>
                    <td>{row.amount}</td>
                </tr>
            );
        }

        console.log("table");
        console.log(table);
        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Borrower</th>
                        <th>Lender</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    {table}
                </tbody>
            </table>
        );
    }
}

class App extends React.Component {
    render() {
        if ("?listpersons" == window.location.search) {
            console.log("entered listpersons");
            return (
                <div>
                    <NavigationBar />
                    <ListPersons />
                </div>
            );
        }
        else if ("?listloans" == window.location.search) {
            console.log("entered listloans");
            return (
                <div>
                    <NavigationBar />
                    <ListLoans />
                </div>
            );
        }
        else if ("?addperson" == window.location.search) {
            console.log("entered addperson");
            return (
                <div>
                    <NavigationBar />
                    <AddPerson />
                    <ListPersons />
                </div>
            );
        }
        else if ("?addloan" == window.location.search) {
            console.log("entered addperson");
            return (
                <div>
                    <NavigationBar />
                    <AddLoan />
                    <ListPersons />
                </div>
            );
        }
        else {
            return (
                <div>
                    <NavigationBar />
                    <h1>Welcome</h1>
                </div>
            );
        }
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('root')
);

