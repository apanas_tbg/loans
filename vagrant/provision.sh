#!/usr/bin/env bash

SYSTEM=$1
IP=$2

if [ -z "${SYSTEM}" ] || [ -z "${IP}" ]
then
    echo "Run script with system name and IP as parameters"
    exit 1
fi

GREEN='\033[0;32m'
NC='\033[0m'

# stop Loans and MariaDB service in re-provision case
sudo systemctl stop loans.service >/dev/null 2>/dev/null
sudo systemctl stop mariadb.service >/dev/null 2>/dev/null

echo "Installing MariaDB..."
sudo yum install -y mariadb-server

echo "Installing Java 1.8.0 OpenJDK..."
sudo yum install -y java-1.8.0-openjdk

echo "Installing GIT..."
sudo yum install -y git

echo "Setting timezone..."
timedatectl set-timezone Europe/Warsaw

echo "Configuring network..."
sudo ifconfig eth1 ${IP}/24 up

echo "Starting MariaDB service..."
sudo systemctl start mariadb.service

echo "Setting MariaDB root password..."
mysql -uroot -e "" >/dev/null 2>&1
if [ $? -eq 0 ]
then
    echo  "SET PASSWORD FOR 'root'@'localhost' = PASSWORD('test');" | mysql -u root
else
    echo  "Unable to set root password - already set"
fi

echo "Creating ${SYSTEM} system directorys"
sudo mkdir -p /vagrant/system
sudo chmod 777 /vagrant/system
sudo rm -rf /vagrant/system/${SYSTEM}
sudo rm -f /project
sudo mkdir -p /vagrant/system/${SYSTEM}
sudo ln -sf /vagrant/system/${SYSTEM} /project

echo "Cloning repository to exclusive use by ${SYSTEM} system"
cd /project
git clone /vagrant/.git ./

echo "Switching to ${SYSTEM} branch"
cd /project
git checkout ${SYSTEM}

echo "Importing project database..."
mysql -uroot -ptest < /project/database/mariadb.sql

echo "Installing Loans service..."
cp /project/server/loans.service /etc/systemd/system/
systemctl daemon-reload
systemctl start loans.service

echo -e "To access guest via ssh run ${GREEN}vagrant ssh ${SYSTEM}${NC}"
echo -e "${SYSTEM^} version is available on ${SYSTEM} system at ${GREEN}/project${NC} directory"
echo -e "Application is available at ${GREEN}http://${IP}:8080/${NC}"

