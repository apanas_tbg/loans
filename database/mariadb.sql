drop database if exists school_loans;

create database school_loans;
use school_loans;

create table person(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name varchar(120),
    primary key(id)
);

create table loan(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    amount DECIMAL(10,2),
    borrower_id INT UNSIGNED NOT NULL,
    lender_id INT UNSIGNED NOT NULL,
    primary key(id),
    foreign key(borrower_id) references person(id),
    foreign key(lender_id) references person(id)
);

INSERT INTO `person` VALUES (1, 'Monika');
INSERT INTO `person` VALUES (2, 'Adrian');
INSERT INTO `person` VALUES (3, 'Wojtek');

GRANT USAGE ON *.* TO 'school_loans'@'localhost';
DROP USER 'school_loans'@'localhost';

CREATE USER 'school_loans'@'localhost' IDENTIFIED BY 'school_loans_pass';
GRANT SELECT, INSERT ON school_loans.* TO 'school_loans'@'localhost';
