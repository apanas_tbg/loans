package school_loans;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class LoansApp {
    public static String PATH_TO_FRONTEND = "../frontend";
    public static int PORT_NUMBER = 8080;

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/school_loans";

    //  Database credentials
    static final String USER = "school_loans";
    static final String PASS = "school_loans_pass";

    public static void main(String[] args) throws Exception {
        if (1 <= args.length) {
            PORT_NUMBER = Integer.parseInt(args[0]);
        }

        if (2 <= args.length) {
            System.out.println(args[1]);
            PATH_TO_FRONTEND = args[1];
        }
        System.out.println("Port: " + Integer.toString(PORT_NUMBER));
        Path frontendPath = Paths.get(System.getProperty("user.dir"));
        frontendPath = frontendPath.resolve(PATH_TO_FRONTEND);
        System.out.println("Frontend path: " +  frontendPath.toAbsolutePath());

        HttpServer server = HttpServer.create(new InetSocketAddress(PORT_NUMBER), 0);
        server.createContext("/", new RawFileHandler());
        server.createContext("/getloans/", new GetLoansHandler());
        server.createContext("/getpersons/", new GetPersonsHandler());
        server.createContext("/addloan/", new AddLoanHandler());
        server.createContext("/addperson/", new AddPersonHandler());
        server.setExecutor(null); // creates a default executor
        server.start();
    }

    static class RawFileHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String path = t.getRequestURI().getRawPath();

            // "path" have "/" at the beginning
            String fullPath;
            if (path.length() == 1) {
                // "index.htm"
                fullPath = PATH_TO_FRONTEND + "/index.htm";
            }

            // anti-hacking:
            else if (path.contains(".."))
            {
                String response = "Hacking attempt!";
                t.sendResponseHeaders(404, response.length());
                OutputStream os = t.getResponseBody();
                os.write(response.getBytes());
                os.close();
                return;
            }
            else {
                fullPath = PATH_TO_FRONTEND + path;
            }
            File file = new File(fullPath);

            if (!file.exists()) {
                String response = "404 Cannot find file: " + path;
                t.sendResponseHeaders(404, response.length());
                OutputStream os = t.getResponseBody();
                os.write(response.getBytes());
                os.close();
                return;
            }
            t.sendResponseHeaders(200, file.length());
            OutputStream os = t.getResponseBody();
            Files.copy(file.toPath(), os);
            os.close();
        }
    }

    static class AddLoanHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "";
            int code = 200;

            System.out.print("Start of handling");
            String parameters = t.getRequestURI().getSchemeSpecificPart();
            String [] parametersAsArray = parameters.split("/");
            // variable "parameteres" starts from "/", so first (0) element is empty.
            // Second (1) element is name of command
            // Third (2) element is parameter
            String lender_id = parametersAsArray[2];
            String borrower_id = parametersAsArray[3];
            String amount = parametersAsArray[4];

            Connection conn = null;
            Statement stmt = null;

            try {
                // It requires to have JAR of "MySQL Connector/J" being used. Configure IDE please.
                // If configured - remember to refresh workspace.
                Class.forName("com.mysql.jdbc.Driver");

                System.out.println("Connecting to database...");
                conn = DriverManager.getConnection(DB_URL, USER, PASS);

                System.out.println("Creating statement...");
                stmt = conn.createStatement();
                String sqlCommand = "INSERT INTO loan(lender_id, borrower_id, amount) values ("
                    + "'" + lender_id + "', "
                    + "'" + borrower_id + "', "
                    + amount + ")";
                int affectedRows = stmt.executeUpdate(sqlCommand);

                // Remember, that it is possible that user was created previously.
                if (1 != affectedRows) {
                    System.out.println("Problem with executing update");
                    response = "Cannot add person";
                    code = 501; 
                }
                else {
                    response = "OK";
                    code = 200;
                }

                stmt.close();
                conn.close();
                System.out.println("End of executing statement");
            }
            catch(SQLException e) {
                System.out.println("Problem with executing update (sql exception)");
                response = "Cannot add person";
                code = 501;
                e.printStackTrace();
            }
            catch(Exception e) {
                System.out.println("Problem with executing update (exception)");
                response = "Cannot add person";
                code = 501;
                e.printStackTrace();
            }
            finally {
                try {
                    if(stmt!=null) {
                        stmt.close();
                    }
                }
                catch(SQLException se2) {
                 // nothing we can do
                }
    
                try {
                    if(conn!=null) {
                        conn.close();
                    }
                }
                catch(SQLException se) {
                    se.printStackTrace();
                }
            }
            System.out.println("End of handling");

            t.sendResponseHeaders(code, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class AddPersonHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "";
            int code = 200;

            System.out.print("Start of handling");
            String parameters = t.getRequestURI().getSchemeSpecificPart();
            String [] parametersAsArray = parameters.split("/");
            // variable "parameteres" starts from "/", so first (0) element is empty.
            // Second (1) element is name of command
            // Third (2) element is parameter
            String personName = parametersAsArray[2];

            Connection conn = null;
            Statement stmt = null;

            try {
                // It requires to have JAR of "MySQL Connector/J" being used. Configure IDE please.
                // If configured - remember to refresh workspace.
                Class.forName("com.mysql.jdbc.Driver");

                System.out.println("Connecting to database...");
                conn = DriverManager.getConnection(DB_URL, USER, PASS);

                System.out.println("Creating statement...");
                stmt = conn.createStatement();
                String sqlCommand = "INSERT INTO person(name) values ('" + personName + "')";
                int affectedRows = stmt.executeUpdate(sqlCommand);

                // Remember, that it is possible that user was created previously.
                if (0 != affectedRows && 1 != affectedRows) {
                    System.out.println("Problem with executing update");
                    response = "Cannot add person";
                    code = 501; 
                }
                else {
                    response = "OK";
                    code = 200;
                }

                stmt.close();
                conn.close();
                System.out.println("End of executing statement");
            }
            catch(SQLException e) {
                System.out.println("Problem with executing update (sql exception)");
                response = "Cannot add person";
                code = 501;
                e.printStackTrace();
            }
            catch(Exception e) {
                System.out.println("Problem with executing update (exception)");
                response = "Cannot add person";
                code = 501;
                e.printStackTrace();
            }
            finally {
                try {
                    if(stmt!=null) {
                        stmt.close();
                    }
                }
                catch(SQLException se2) {
                 // nothing we can do
                }
    
                try {
                    if(conn!=null) {
                        conn.close();
                    }
                }
                catch(SQLException se) {
                    se.printStackTrace();
                }
            }
            System.out.println("End of handling");

            t.sendResponseHeaders(code, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class GetLoansHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "";
            int code = 200;
            System.out.print("Start of handling");

            Connection conn = null;
            Statement stmt = null;

            try {
                // It requires to have JAR of "MySQL Connector/J" being used. Configure IDE please.
                // If configured - remember to refresh workspace.
                Class.forName("com.mysql.jdbc.Driver");

                System.out.println("Connecting to database...");
                conn = DriverManager.getConnection(DB_URL, USER, PASS);

                System.out.println("Creating statement...");
                stmt = conn.createStatement();
                String sqlCommand =
                    "SELECT l.id, l.amount, p1.name borrower, p2.name lender FROM loan l"
                    + " LEFT JOIN person p1 on l.borrower_id = p1.id"
                    + " LEFT JOIN person p2 on l.lender_id = p2.id";
                ResultSet rs = stmt.executeQuery(sqlCommand);

                response = "[\n";
                code = 200;

                while (rs.next()) {
                    int id = rs.getInt("l.id");
                    String amount = rs.getString("l.amount");
                    String borrower = rs.getString("borrower");
                    String lender = rs.getString("lender");
                    response += "{\"id\":" + Integer.toString(id)
                        + ", \"amount\":\"" + amount + "\""
                        + ", \"borrower\":\"" + borrower + "\""
                        + ", \"lender\":\"" + lender + "\""
                        + "}";
                    if (!rs.isLast()) {
                        response += ",\n";
                    }
                }
                response += "\n]";

                stmt.close();
                conn.close();
                System.out.println("End of executing statement");
            }
            catch(SQLException e) {
                System.out.println("Problem with executing update (sql exception)");
                response = "Cannot add person";
                code = 501;
                e.printStackTrace();
            }
            catch(Exception e) {
                System.out.println("Problem with executing update (exception)");
                response = "Cannot add person";
                code = 501;
                e.printStackTrace();
            }
            finally {
                try {
                    if(stmt!=null) {
                        stmt.close();
                    }
                }
                catch(SQLException se2) {
                 // nothing we can do
                }
    
                try {
                    if(conn!=null) {
                        conn.close();
                    }
                }
                catch(SQLException se) {
                    se.printStackTrace();
                }
            }
            System.out.println("End of handling");

            t.sendResponseHeaders(code, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class GetPersonsHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "";
            int code = 200;
            System.out.print("Start of handling");
            t.getResponseHeaders().set("Content-Type", String.format("application/json; charset=%s",
                StandardCharsets.UTF_8));

            Connection conn = null;
            Statement stmt = null;

            try {
                // It requires to have JAR of "MySQL Connector/J" being used. Configure IDE please.
                // If configured - remember to refresh workspace.
                Class.forName("com.mysql.jdbc.Driver");

                System.out.println("Connecting to database...");
                conn = DriverManager.getConnection(DB_URL, USER, PASS);

                System.out.println("Creating statement...");
                stmt = conn.createStatement();
                String sqlCommand = "SELECT p.id, p.name FROM person p";
                ResultSet rs = stmt.executeQuery(sqlCommand);

                response = "[\n";
                code = 200;

                while (rs.next()) {
                    int id = rs.getInt("p.id");
                    String name = rs.getString("p.name");
                    response += "{\"id\":" + Integer.toString(id)
                        + ", \"name\":\"" + name + "\""
                        + "}";
                    if (!rs.isLast()) {
                        response += ",\n";
                    }
                }
                response += "\n]";

                stmt.close();
                conn.close();
                System.out.println("End of executing statement");
            }
            catch(SQLException e) {
                System.out.println("Problem with executing update (sql exception)");
                response = "Cannot add person";
                code = 501;
                e.printStackTrace();
            }
            catch(Exception e) {
                System.out.println("Problem with executing update (exception)");
                response = "Cannot add person";
                code = 501;
                e.printStackTrace();
            }
            finally {
                try {
                    if(stmt!=null) {
                        stmt.close();
                    }
                }
                catch(SQLException se2) {
                 // nothing we can do
                }
    
                try {
                    if(conn!=null) {
                        conn.close();
                    }
                }
                catch(SQLException se) {
                    se.printStackTrace();
                }
            }
            System.out.println("End of handling");

            t.sendResponseHeaders(code, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes(StandardCharsets.UTF_8));
            os.close();
        }
    }
}
